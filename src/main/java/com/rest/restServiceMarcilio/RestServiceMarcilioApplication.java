package com.rest.restServiceMarcilio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestServiceMarcilioApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestServiceMarcilioApplication.class, args);
	}

}
