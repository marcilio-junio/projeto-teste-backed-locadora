package com.rest.restServiceMarcilio.response;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class ResponseHandler {

    //Modo de criar um padrão para apresentar mensagem de body response exemplo:(mensagem: "teste"   Status: "500")
    public static ResponseEntity<Object> generateResponse(String message, HttpStatus status){
        Map<String, Object> map = new HashMap<>();
        map.put("message", message);
        map.put("status", status.value());
        return new ResponseEntity<Object>(map, status);
    }
}
