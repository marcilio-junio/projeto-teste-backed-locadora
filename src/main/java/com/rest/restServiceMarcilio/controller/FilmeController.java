package com.rest.restServiceMarcilio.controller;

import com.rest.restServiceMarcilio.model.Filme;
import com.rest.restServiceMarcilio.repository.FilmeRepository;
import com.rest.restServiceMarcilio.response.ResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
public class FilmeController {

    //comando usando para utilizar comandos já prontos na classe repository
    @Autowired
    private FilmeRepository filmeRepository;

    //realiza o link de pesquisa
    @GetMapping(path = "/filme/{codigo}")
    //passando o pathvariable estou garantindo que ele está buscando o codigo adicionando no path acima
    public ResponseEntity consultaFilmePorId(@PathVariable("codigo") Integer codigo){
        return filmeRepository.findById(codigo)
                .map(record -> ResponseEntity.ok().body(record))
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(path = "/filmes")
    public ResponseEntity<Iterable<Filme>> listarTodosOsFilmes(){
        return ResponseEntity.status(HttpStatus.OK).body(filmeRepository.findAll());
    }

    @PostMapping(path = "/cadastrar-filme")
    public ResponseEntity criarFilme(@RequestBody Filme filme) {
        if (filmeRepository.existsById(filme.getCodigo())) {
            return ResponseHandler.generateResponse("Filme já cadastrado", HttpStatus.CONFLICT);
        }

        if (filme.getNome().equals(null) || filme.getNome().isBlank() || filme.getNome().isEmpty()) {
            return ResponseHandler.generateResponse("Nome do filme é obrigatorio", HttpStatus.CONFLICT);
        }

        //Caso o filme não esteja cadastrado, vou add o novo item
        var objetoFilme = new Filme();

        //Informando os campos que serão preenchidos.
        objetoFilme.setCodigo(filme.getCodigo());
        objetoFilme.setAnoLancamento(filme.getAnoLancamento());
        objetoFilme.setFaixaEtaria(filme.getFaixaEtaria());
        objetoFilme.setGenero(filme.getGenero());
        objetoFilme.setNome(filme.getNome());
        objetoFilme.setSinopse(filme.getSinopse());

        //Salvar os dados nos campos correspondetes.
        return ResponseEntity.status(HttpStatus.CREATED).body(filmeRepository.save(objetoFilme));
    }

    //Deletar Filme pelo numero do codigo
    @DeleteMapping(path = "/filme/{codigo}")
    public void deletarFilmePorId(@PathVariable("codigo") Integer codigo){
        filmeRepository.findById(codigo)
                .map(record -> {
                    filmeRepository.deleteById(codigo);
                    return Void.TYPE;
                }).orElseThrow(() -> new ResponseStatusException(HttpStatus.OK));
    }

    //Alterar dados de um item
    @PutMapping(path = "/filme/{codigo}")
    public ResponseEntity editarFilmePut(@PathVariable("codigo") Integer codigo, @RequestBody Filme filme) {
        Optional<Filme> filmeObject = filmeRepository.findById(codigo);

        if(!filmeObject.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\n" +
                    "     \"message\": \"Filme não encontrado\",\n" +
                    "}");
        }

        //adicionar validações depois para os items.
        var filmeEditado = filmeObject.get();
        filmeEditado.getCodigo();
        filmeEditado.setNome(filme.getNome());
        filmeEditado.setFaixaEtaria(filme.getFaixaEtaria());
        filmeEditado.setAnoLancamento(filme.getAnoLancamento());
        filmeEditado.setGenero(filme.getGenero());
        filmeEditado.setSinopse(filme.getSinopse());

        return  ResponseEntity.status(HttpStatus.OK).body(filmeRepository.save(filmeEditado));
    }

    //Alterar Itens separado de um dado
    @PatchMapping(path = "/filme/{codigo}")
    public ResponseEntity editarFilmePatch(@PathVariable("codigo") Integer codigo, @RequestBody Filme filme) {
        Optional<Filme> filmeObject = filmeRepository.findById(codigo);

        if(!filmeObject.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\n" +
                    "     \"message\": \"Filme não encontrado\",\n" +
                    "}");
        }

        //Depois tentar fazer validações para alterar um item, como não deixar em branco, ter um item a alterar, campo null
        var filmeEditado = filmeObject.get();
        filmeEditado.getCodigo();

        if (filme.getNome() == null){
            filmeEditado.getNome();
        }else {
            filmeEditado.setNome(filme.getNome());
        }

        if (filme.getFaixaEtaria() == null){
            filmeEditado.getFaixaEtaria();
        }else {
            filmeEditado.setFaixaEtaria(filme.getFaixaEtaria());
        }

        if (filme.getAnoLancamento()== null){
            filmeEditado.getAnoLancamento();
        }else {
            filmeEditado.setAnoLancamento(filme.getAnoLancamento());
        }

        if (filme.getGenero() == null){
            filmeEditado.getGenero();
        }else {
            filmeEditado.setGenero(filme.getGenero());
        }

        if (filme.getSinopse() == null){
            filmeEditado.getSinopse();
        }else {
            filmeEditado.setSinopse(filme.getSinopse());
        }

        return  ResponseEntity.status(HttpStatus.OK).body(filmeRepository.save(filmeEditado));
    }
}
