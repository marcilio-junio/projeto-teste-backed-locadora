package com.rest.restServiceMarcilio.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

//Vai conter as nossas rotas "Post", "Delet", "PUT"
@RestController
public class StatusController {

    //Quando bato nessa rota ele trás oq está apontado nela.
    @GetMapping(path = "/status")
    public String statusApi(){
        return "A Aplicação está Online";
    }


}

