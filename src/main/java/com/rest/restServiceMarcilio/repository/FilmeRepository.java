package com.rest.restServiceMarcilio.repository;

import com.rest.restServiceMarcilio.model.Filme;
import org.springframework.data.repository.CrudRepository;

public interface FilmeRepository extends CrudRepository<Filme, Integer> {
}
