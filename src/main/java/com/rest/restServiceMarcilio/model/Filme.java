package com.rest.restServiceMarcilio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Filme {
    @Id
    @Column(name = "codigo", nullable = false)
    private Integer codigo;

    @Column(name = "nome", length = 100, nullable = false)
    private String nome;

    @Column(name = "anoLancamento", length = 4, nullable = false)
    private Integer anoLancamento;

    @Column(name = "sinopse", length = 100)
    private String sinopse;

    @Column(name = "faixaEtaria", length = 2)
    private Integer faixaEtaria;

    @Column(name = "genero", length = 100, nullable = false)
    private String genero;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getAnoLancamento() {
        return anoLancamento;
    }

    public void setAnoLancamento(Integer anoLancamento) {
        this.anoLancamento = anoLancamento;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public Integer getFaixaEtaria() {
        return faixaEtaria;
    }

    public void setFaixaEtaria(Integer faixaEtaria) {
        this.faixaEtaria = faixaEtaria;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }
}
